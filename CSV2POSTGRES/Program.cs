﻿
using System.Reflection;
using Npgsql;
using Microsoft.Extensions.Configuration;

namespace CSV2POSTGRES
{
    class Program
    {
      
        static void Main(string[] args)
        {
            // Configuration setup
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();

            // Read connection string from appsettings.json
            string connectionString = configuration.GetSection("ConnectionString").Value;

            var assemblyDirectory = Path.GetDirectoryName(Assembly.GetAssembly(typeof(Program)).Location);
            var dataDirectory = Path.Combine(assemblyDirectory, "CSV");

            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();
                /*
                string copyCommand = @"COPY ""InstallmentFeeExclusionList""(""ConsumerId"", ""AccountId"" , ""IsStaff"") FROM STDIN (FORMAT csv, HEADER true)";
                */
                
                string copyCommand = @"COPY ""InstallmentFeeExclusionList""(""ConsumerId"", ""AccountId"", ""DateAdded"") FROM STDIN (FORMAT csv, HEADER true)";

                // Iterate through each CSV file in the Data directory
                foreach (var filePath in Directory.GetFiles(dataDirectory, "*.csv"))
                {
                    Console.WriteLine($"Processing file: {filePath}");

                    // Execute the COPY command
                    using (var writer = connection.BeginTextImport(copyCommand))
                    using (var reader = new StreamReader(filePath))
                    {
                        while (!reader.EndOfStream)
                        {
                            var line = reader.ReadLine();
                            var lineWithDate =line + $",\"{DateTime.Now:yyyy-MM-dd HH:mm:ss}\"";
                            writer.WriteLine(lineWithDate);
                            
                        }
                    }

                    Console.WriteLine($"Data from {filePath} has been successfully loaded into PostgreSQL.");
                }
            }
        }
    }
}

